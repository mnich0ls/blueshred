//
//  ShredPeripheral.swift
//
//  Created by Michael Nichols on 3/8/23.
//

import Foundation
import CoreBluetooth

// TODO: This class should track the connection state e.g. can be connected/disconnected
public protocol ShredPeripheralProtocol {
    
    /// An identifier used in logging and callbacks to the delegate
    /// We use a UUID here instead of anything tied to the `CoreBluetooth` library to make
    /// stubbing/mocking simpler for testing
    var identifier: UUID { get }
    
    /// The advertisement data captured before the connection
    var advertisementData: [String: Any]? { get }
    
    /// Update the notification value to `true` for the given characteristic
    /// Will throw an error if the characteristic could not be discovered or if an existing operation
    /// to enable notifications on the characteristic is already in progress
    func enableNotifications(for char: ShredCharacteristic) async throws
    
    /// Write the `value` to the characteristic
    /// Will attempt to discover characteristic if it has not yet been discovered
    /// Will throw an error if the write is not completed within the default timeout period - see `PendingWrite`
    func write(_ value: Data, char: ShredCharacteristic) async throws
    
    /// Read the value from the characteristic
    /// Will attempt to discover characteristic if it has not yet been discovered
    /// Will throw an error if the read is not completed within the default timeout period - see `PendingRead`
    func read(char: ShredCharacteristic) async throws -> Data
    
    /// Wait to receive a notification from the characteristic
    func awaitNotification(char: ShredCharacteristic) async throws -> Data
}

/// Delegate handles characteristic notifications
public protocol ShredPeripheralNotificationDelegate {
    func didUpdate(shredCharacteristic: ShredCharacteristic, peripheralIdentifier: UUID)
}

public class ShredPeripheral: NSObject, ShredPeripheralProtocol {
    
    enum WriteError: Error {
        case writeInProgress
        case requestTimeout
    }
    
    enum ReadError: Error {
        case readInProgress
        case receivedNilData
        case requestTimeout
    }
    
    enum NotificationError: Error {
        case notificationEnableInProgress
        case awaitNotificationInProgress
        case requestTimeout
    }
    
    public let identifier: UUID
    
    public let advertisementData: [String : Any]?
    
    public override var description: String {
        "<ShredPeripheral: identifier: \(identifier)>"
    }
    
    // the CBPeripheral is not the same instance from discovery to connection, we need to rethink how we handle this reference
    
    internal var cbPeripheral: CBPeripheral! {
        didSet {
            cbPeripheral.delegate = self
        }
    }
    
    private var services = [CBUUID: CBService]()
    private var chars = [CBUUID: CBCharacteristic]()
    
    private var pendingWrites = [ShredCharacteristic: PendingWrite]()
    private var pendingReads = [ShredCharacteristic: PendingRead]()
    private var pendingEnableNotifications = [ShredCharacteristic: PendingEnableNotification]()
    private var pendingAwaitNotifications = [ShredCharacteristic: PendingAwaitNotification]()
    
    private var serialQueue = DispatchQueue(label: "shred.peripheral")
    
    public init(peripheral: CBPeripheral, identifier: UUID) {
        
        self.identifier = identifier
        self.advertisementData = nil
        
        super.init()
        
        self.cbPeripheral = peripheral
    }
    
    public init(peripheral: CBPeripheral, identifier: UUID, advertisementData: [String: Any]) {
        
        self.cbPeripheral = peripheral
        self.identifier = identifier
        self.advertisementData = advertisementData
        
        super.init()
        
        self.cbPeripheral.delegate = self
    }
    
    public func enableNotifications(for char: ShredCharacteristic) async throws {
        
        let _: () = try await withCheckedThrowingContinuation { continuation in
            
            BlueShred.serialQueue.async {
                if let existing = self.pendingEnableNotifications[char] {
                    if existing.isValid {
                        return continuation.resume(throwing: NotificationError.notificationEnableInProgress)
                    } else {
                        self.pendingEnableNotifications.removeValue(forKey: char)
                    }
                }
                
                self.pendingEnableNotifications[char] = PendingEnableNotification(continuation: continuation)
                
                guard let cbChar = self.getOrDiscoverCBChar(char: char) else { return }
                
                Log.blueShred.debug("Will enable notifications for: \(char.description, privacy: .public)")
                
                self.cbPeripheral.setNotifyValue(true, for: cbChar)
            }
        }
    }
    
    public func write(_ value: Data, char: ShredCharacteristic) async throws {
        
        let _: () = try await withCheckedThrowingContinuation { continuation in
            
            BlueShred.serialQueue.async {
                if let existing = self.pendingWrites[char] {
                    if existing.isValid {
                        Log.blueShred.error("Attempting to write char: \(char.description, privacy: .public), but we currently have a pending write for peripheral: \(self, privacy: .public)")
                        return continuation.resume(throwing: WriteError.writeInProgress)
                    } else {
                        self.pendingWrites.removeValue(forKey: char)
                    }
                }
                
                self.pendingWrites[char] = PendingWrite(value, continuation: continuation)
                
                guard let cbChar = self.getOrDiscoverCBChar(char: char) else { return }
                
                Log.blueShred.debug("Will write value for: \(char.description, privacy: .public)")
                
                // We default to `withResponse`, but this would be a relatively
                // straightforward change if we need `withoutResponse` in the future
                self.cbPeripheral.writeValue(value, for: cbChar, type: .withResponse)
            }
        }
    }
    
    public func read(char: ShredCharacteristic) async throws -> Data {
        
        return try await withCheckedThrowingContinuation { continuation in
            
            BlueShred.serialQueue.async {
                if let existing = self.pendingReads[char] {
                    if existing.isValid {
                        return continuation.resume(throwing: ReadError.readInProgress)
                    } else {
                        self.pendingReads.removeValue(forKey: char)
                    }
                }
                
                self.pendingReads[char] = PendingRead(continuation: continuation)
                
                guard let cbChar = self.getOrDiscoverCBChar(char: char) else { return }
                
                Log.blueShred.debug("Will read value for: \(char.description, privacy: .public)")
                
                self.cbPeripheral.readValue(for: cbChar)
            }
        }
    }
    
    public func awaitNotification(char: ShredCharacteristic) async throws -> Data {
        
        return try await withCheckedThrowingContinuation { continuation in
            
            BlueShred.serialQueue.async {
                
                if let existing = self.pendingAwaitNotifications[char] {
                    if existing.isValid {
                        continuation.resume(throwing: NotificationError.awaitNotificationInProgress)
                    } else {
                        self.pendingReads.removeValue(forKey: char)
                    }
                }
                
                self.pendingAwaitNotifications[char] = PendingAwaitNotification(continuation: continuation)
            }
        }
    }

    // MARK: Private methods
    
    private func getOrDiscoverCBChar(char: ShredCharacteristic) -> CBCharacteristic? {
        
        guard let service = services[char.cbServiceUUID] else {
            Log.blueShred.debug("Will discover service: \(char.description, privacy: .public) for: \(self.identifier, privacy: .public)")
            cbPeripheral.discoverServices([char.cbServiceUUID])
            return nil
        }
        
        guard let cbChar = chars[char.cbCharUUID] else {
            Log.blueShred.debug("Will discover characteristic: \(char.description, privacy: .public) for: \(self.identifier, privacy: .public)")
            cbPeripheral.discoverCharacteristics([char.cbCharUUID], for: service)
            return nil
        }
        
        return cbChar
    }
    
    private func didDiscoverServices(cbServices: [CBService]) {
        
        for cbService in cbServices {
            
            if !services.keys.contains(cbService.uuid) {
                services[cbService.uuid] = cbService
            }
            
            Log.blueShred.debug("Did discover service: \(cbService.uuid, privacy: .public)")
            
            // collect all the needed chars from any waiting continuations
            var chars = [CBUUID]()
            for shredChar in pendingWrites.keys {
                if shredChar.cbServiceUUID == cbService.uuid, !self.chars.keys.contains(shredChar.cbCharUUID) {
                    chars.append(shredChar.cbCharUUID)
                }
            }
            
            cbPeripheral.discoverCharacteristics(chars, for: cbService)
        }
    }
    
    private func didDiscoverCharacteristics(cbChars: [CBCharacteristic]) {
        
        for cbChar in cbChars {
            guard let shredChar = cbChar.shredCharacteristic() else { continue }
            guard !chars.keys.contains(shredChar.cbCharUUID) else { continue }
            
            Log.blueShred.debug("Did discover characteristic: \(shredChar.description, privacy: .public) for: \(self.identifier, privacy: .public)")
            
            chars[shredChar.cbCharUUID] = cbChar
            
            if let pendingWrite = pendingWrites[shredChar] {
                Log.blueShred.debug("Will write: \(shredChar.description, privacy: .public) for: \(self.identifier, privacy: .public)")
                cbPeripheral.writeValue(pendingWrite.data, for: cbChar, type: .withResponse)
            }
            
            if let _ = pendingReads[shredChar] {
                Log.blueShred.debug("Will read: \(shredChar.description, privacy: .public) for: \(self.identifier, privacy: .public)")
                cbPeripheral.readValue(for: cbChar)
            }
            
            if let _ = pendingEnableNotifications[shredChar] {
                Log.blueShred.debug("Will enable notifications: \(shredChar.description, privacy: .public) for: \(self.identifier, privacy: .public)")
                cbPeripheral.setNotifyValue(true, for: cbChar)
            }
        }
    }
    
    private func resumeWriteContinuation(char: CBCharacteristic) {
        
        guard let shredChar = char.shredCharacteristic() else { return }
        
        guard let pendingWrite = pendingWrites[shredChar] else {
            Log.blueShred.error("Did write value for char: \(shredChar.description, privacy: .public), but we have no continuation to call")
            return
        }
        
        pendingWrites.removeValue(forKey: shredChar)
        
        pendingWrite.complete(returning: ())
    }
    
    private func resumeReadContinuation(char: CBCharacteristic) {
        
        guard let shredChar = char.shredCharacteristic() else { return }
        
        guard let pendingRead = pendingReads[shredChar] else { return }
        
        pendingReads.removeValue(forKey: shredChar)
        
        guard let data = char.value else {
            Log.blueShred.error("Did update value for char: \(shredChar.description, privacy: .public), but there was no value received")
            return pendingRead.error(error: ReadError.receivedNilData)
        }
        
        Log.blueShred.debug("Did update value for: \(shredChar.description, privacy: .public)")
        
        pendingRead.complete(returning: data)
    }
    
    private func resumeEnableNotification(char: CBCharacteristic) {
        
        guard let shredChar = char.shredCharacteristic() else { return }
        
        guard let pendingEnableNotification = pendingEnableNotifications[shredChar] else {
            // TODO: Do we need a delegate callback method for this?
            return
        }
        
        pendingEnableNotifications.removeValue(forKey: shredChar)
        
        Log.blueShred.debug("Did update notification value: \(char.isNotifying, privacy: .public) for char: \(shredChar.description, privacy: .public)")
        
        pendingEnableNotification.complete(returning: ())
    }
    
    private func resumeAwaitNotification(char: CBCharacteristic) {
        
        guard let shredChar = char.shredCharacteristic() else { return }
        
        guard let pending = pendingAwaitNotifications[shredChar] else { return }
        
        guard let value = char.value else {
            Log.blueShred.warning("Received notification from char: \(char.description, privacy: .public) with no value")
            return
        }
        
        pendingAwaitNotifications.removeValue(forKey: shredChar)
        
        Log.blueShred.debug("Did receive notification for char: \(shredChar.description, privacy: .public)")
        
        pending.complete(returning: value)
    }
}

extension ShredPeripheral {
    
    public override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? ShredPeripheral else {
            return false
        }
        return other.identifier == identifier
    }
    
    public static func == (lhs: ShredPeripheral, rhs: ShredPeripheral) -> Bool {
        return (lhs.identifier == rhs.identifier)
    }
    
    override public var hash: Int {
        identifier.hashValue
    }
}


extension ShredPeripheral: CBPeripheralDelegate {
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        
        DispatchQueue.main.async {
            self.resumeEnableNotification(char: characteristic)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let cbServices = peripheral.services else { return }
        
        DispatchQueue.main.async {
            self.didDiscoverServices(cbServices: cbServices)
        }
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        Log.blueShred.debug("Did discover characteristics for: \(peripheral.identifier, privacy: .public)")
        
        guard let cbChars = service.characteristics else { return }
        
        DispatchQueue.main.async {
            self.didDiscoverCharacteristics(cbChars: cbChars)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
        DispatchQueue.main.async {
            self.resumeWriteContinuation(char: characteristic)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        DispatchQueue.main.async {
            self.resumeReadContinuation(char: characteristic)
            self.resumeAwaitNotification(char: characteristic)
        }
    }
}

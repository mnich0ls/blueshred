//
//  PendingEnableNotification.swift
//  
//
//  Created by Michael Nichols on 3/10/23.
//

import Foundation

class PendingEnableNotification: PendingOperation<Void> {
    
    override init(continuation: CheckedContinuation<Void, Error>) {
        super.init(continuation: continuation)
        
        operationDescription = "Enable Notifications"
    }
}

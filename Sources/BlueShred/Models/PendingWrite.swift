//
//  PendingWrite.swift
//  
//
//  Created by Michael Nichols on 3/9/23.
//

import Foundation

class PendingWrite: PendingOperation<Void> {
    
    let data: Data
    
    required init(_ data: Data, continuation: CheckedContinuation<Void, Error>) {
        self.data = data
        
        super.init(continuation: continuation)
        
        operationDescription = "Write Value"
    }
}

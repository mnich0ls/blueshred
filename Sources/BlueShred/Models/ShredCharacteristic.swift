//
//  ShredCharacteristic.swift
//  
//
//  Created by Michael Nichols on 3/9/23.
//

import Foundation
import CoreBluetooth

public struct ShredCharacteristic: Hashable {
    
    public var serviceUUID: String
    public var charUUID: String
    
    public var cbServiceUUID: CBUUID {
        CBUUID(string: serviceUUID)
    }
    
    public var cbCharUUID: CBUUID {
        CBUUID(string: charUUID)
    }
    
    public var description: String {
        get {
            _description ?? charUUID
        }
    }
    
    private let _description: String?
    
    init(serviceUUID: String, charUUID: String) {
        self.serviceUUID = serviceUUID
        self.charUUID = charUUID
        self._description = nil
    }
    
    public init(serviceUUID: String, charUUID: String, description: String) {
        self.serviceUUID = serviceUUID
        self.charUUID = charUUID
        self._description = description
    }
    
    public static func == (lhs: ShredCharacteristic, rhs: ShredCharacteristic) -> Bool {
        return (lhs.serviceUUID.lowercased() == rhs.serviceUUID.lowercased()) &&
        (lhs.charUUID.lowercased() == rhs.charUUID.lowercased())
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(serviceUUID.lowercased())
        hasher.combine(charUUID.lowercased())
    }
}

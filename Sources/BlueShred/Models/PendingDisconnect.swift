//
//  PendingDisconnect.swift
//  
//
//  Created by Michael Nichols on 3/15/23.
//

import Foundation

class PendingDisconnect: PendingOperation<Void> {
    
    override init(continuation: CheckedContinuation<Void, Error>) {
        super.init(continuation: continuation)
        
        operationDescription = "Disconnect from Peripheral"
    }
}

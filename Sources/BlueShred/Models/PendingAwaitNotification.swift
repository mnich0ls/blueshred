//
//  PendingAwaitNotification.swift
//  
//
//  Created by Michael Nichols on 3/15/23.
//

import Foundation

class PendingAwaitNotification: PendingOperation<Data> {
    
    override init(continuation: CheckedContinuation<Data, Error>) {
        super.init(continuation: continuation)
        
        operationDescription = "Await Notification"
    }
}

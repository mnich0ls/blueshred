//
//  PendingConnection.swift
//  
//
//  Created by Michael Nichols on 3/11/23.
//

import Foundation

class PendingConnection: PendingOperation<Void> {
    
    override init(continuation: CheckedContinuation<Void, Error>) {
        super.init(continuation: continuation)
        
        operationDescription = "Connect to Peripheral"
    }
}

//
//  PendingOperation.swift
//  
//
//  Created by Michael Nichols on 3/11/23.
//

import Foundation

enum PendingOperationError: Swift.Error {
    case timeout
}

class PendingOperation<T> {
    
    private var defaultTimeoutPeriod: TimeInterval = 10.0
    
    private(set) var isValid: Bool = true
    
    private let continuation: CheckedContinuation<T, Error>
    private var timer: Timer!
    
    internal var operationDescription: String
    
    init(continuation: CheckedContinuation<T, Error>) {
        self.continuation = continuation
        self.operationDescription = "N/A - (did you forget to provide a description?)"
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(withTimeInterval: self.defaultTimeoutPeriod, repeats: false) { [weak self] _ in
                guard let self = self else { return }
                Log.blueShred.error("Pending operation: \(self.operationDescription, privacy: .public) did timeout")
                self.isValid = false
                self.continuation.resume(throwing: PendingOperationError.timeout)
            }
        }
    }
    
    deinit {
        Log.blueShred.debug("Deinit pending operation: \(self.operationDescription, privacy: .public)")
    }
    
    func complete(returning: T) {
        DispatchQueue.main.async {
            guard self.isValid else {
                Log.blueShred.error("Attempting to complete pending operation: \(self.operationDescription, privacy: .public) that is no longer valid")
                return
            }
            self.isValid = false
            self.timer.invalidate()
            self.continuation.resume(returning: returning)
        }
    }
    
    func error(error: Error) {
        DispatchQueue.main.async {
            self.isValid = false
            self.timer.invalidate()
            self.continuation.resume(throwing: error)
        }
    }
}

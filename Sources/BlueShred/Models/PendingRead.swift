//
//  PendingRead.swift
//  
//
//  Created by Michael Nichols on 3/9/23.
//

import Foundation

class PendingRead: PendingOperation<Data> {
    
    override init(continuation: CheckedContinuation<Data, Error>) {
        super.init(continuation: continuation)
        
        operationDescription = "Read Value"
    }
}

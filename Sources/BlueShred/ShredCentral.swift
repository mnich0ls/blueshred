//
//  ShredCentral.swift
//  
//
//  Created by Michael Nichols on 3/11/23.
//

import Foundation
import CoreBluetooth

public protocol ShredCentralProtocol {
    
    var state: ShredCentral.State { get async }
    var connections: Set<ShredPeripheral> { get async }
    
    func startScanning(forDevicesWith services: [String]?, allowDuplicates: Bool) async
    func connect(shredPeripheral: ShredPeripheral) async throws
    func disconnect(shredPeripheral: ShredPeripheral) async throws
    func stopScanning() async
}

public protocol ShredCentralDelegate {
    
    func didChangeState(state: ShredCentral.State)
    func didDiscoverPeripheral(peripheral: ShredPeripheral, rssi: NSNumber)
}

public actor ShredCentral: NSObject, ShredCentralProtocol {
    
    public enum Error: Swift.Error {
        case alreadyConnectedToPeripheral
        case pendingPeripheralConnectionExists
    }
    
    public enum State {
        case poweredOn
        case poweredOff
        case unauthorized
        case unsupported
        case resetting
        case unknown
        
        init(cbManagerState: CBManagerState) {
            switch cbManagerState {
            case .poweredOn:
                self = .poweredOn
            case .poweredOff:
                self = .poweredOff
            case .unauthorized:
                self = .unauthorized
            case .unsupported:
                self = .unsupported
            case .resetting:
                self = .resetting
            case .unknown:
                self = .unknown
            default:
                Log.blueShred.warning("Unexpected CBManagerState: \(cbManagerState.rawValue, privacy: .public), returning `unknown`")
                self = .unknown
            }
        }
    }
    
    internal(set) public var state: State = .unknown
    private(set) public var connections = Set<ShredPeripheral>()
    
    internal let delegate: ShredCentralDelegate
    private var bridge: ShredCentralManagerProxy!
    
    public init(delegate: ShredCentralDelegate) {
        
        self.delegate = delegate
        
        super.init()
        
        self.bridge = ShredCentralManagerProxy(central: self)
    }
    
    public func startScanning(forDevicesWith services: [String]?, allowDuplicates: Bool) async {
    
        let options: [String: Any] = [CBCentralManagerScanOptionAllowDuplicatesKey: allowDuplicates]
        
        var cbServices: [CBUUID]?
        
        if let services = services {
            cbServices = [CBUUID]()
            for service in services {
                cbServices!.append(CBUUID(string: service))
            }
        }
        
        bridge.startScanning(cbServices: cbServices, options: options)
    }
    
    public func connect(shredPeripheral: ShredPeripheral) async throws {
        
        guard !connections.contains(shredPeripheral) else {
            Log.blueShred.warning("Already connected to peripheral: \(shredPeripheral, privacy: .public)")
            throw Error.alreadyConnectedToPeripheral
        }
        
        try await bridge.connect(shredPeripheral: shredPeripheral)
        connections.insert(shredPeripheral)
    }
    
    public func disconnect(shredPeripheral: ShredPeripheral) async throws {
        
        try await bridge.disconnect(shredPeripheral: shredPeripheral)
        connections.remove(shredPeripheral)
    }
    
    public func stopScanning() {
        
        bridge.stopScanning()
        // TODO: clear any cached state related to scanning
    }
    
    func bridgeDidUpdateState(state: CBManagerState) {
        
        self.state = ShredCentral.State(cbManagerState: state)
        delegate.didChangeState(state: self.state)
    }
}

/// This class proxies access to the central manager and handles CBCentralManger delegate callback methods
/// The main purpose for splitting this out into a separate class (instead of making this an extension of the ShredCentral
/// actor) is that actor's cannot conform to the CBCentralManager delegate protocol.
///
/// This class is where we manage our continuations to ultimately provide an async interface to some of the CoreBluetooth stack
///
/// This class does not need to provide public access, so it is tightly coupled to the ShredCentral actor
internal class ShredCentralManagerProxy: NSObject, CBCentralManagerDelegate {
    
    private var cbCentralManager: CBCentralManager!
    private let central: ShredCentral
    private var pendingConnections = [ShredPeripheral: PendingConnection]()
    private var pendingDisconnects = [ShredPeripheral: PendingDisconnect]()
    
    init(central: ShredCentral) {
        self.central = central
        
        super.init()
        
        self.cbCentralManager = CBCentralManager.init(delegate: self, queue: BlueShred.serialQueue)
    }
    
    func startScanning(cbServices: [CBUUID]?, options: [String: Any]) {
        
        guard cbCentralManager.state == CBManagerState.poweredOn else { return }
        guard !cbCentralManager.isScanning else { return }
        
        cbCentralManager.scanForPeripherals(withServices: cbServices, options: options)
    }
    
    public func stopScanning() {
        
        if cbCentralManager.state == .poweredOn {
            cbCentralManager.stopScan()
        }
        // TODO: Stop scanning
        // TODO: clear any cached state related to scanning
    }
    
    func connect(shredPeripheral: ShredPeripheral) async throws {
        
        let _: () = try await withCheckedThrowingContinuation { continuation in
            
            BlueShred.serialQueue.async {
                if let pendingConnection = self.pendingConnections[shredPeripheral] {
                    if pendingConnection.isValid {
                        Log.blueShred.error("Already attempting to connect to peripheral: \(shredPeripheral, privacy: .public)")
                        return continuation.resume(throwing: ShredCentral.Error.pendingPeripheralConnectionExists)
                    }
                    self.pendingConnections.removeValue(forKey: shredPeripheral)
                }
                
                Log.blueShred.debug("Will connect to peripheral: \(shredPeripheral, privacy: .public)")
                
                let pendingConnection = PendingConnection(continuation: continuation)
                self.pendingConnections[shredPeripheral] = pendingConnection
                
                self.cbCentralManager.connect(shredPeripheral.cbPeripheral)
            }
        }
    }
    
    func disconnect(shredPeripheral: ShredPeripheral) async throws {
        
        let _: () = try await withCheckedThrowingContinuation { continuation in
            BlueShred.serialQueue.async {
                if let pending = self.pendingDisconnects[shredPeripheral] {
                    if pending.isValid {
                        Log.blueShred.error("Already attempting to disconnect from peripheral: \(shredPeripheral, privacy: .public)")
                    }
                    self.pendingDisconnects.removeValue(forKey: shredPeripheral)
                }
                
                Log.blueShred.debug("Will disconnect from peripheral: \(shredPeripheral, privacy: .public)")
                
                let pending = PendingDisconnect(continuation: continuation)
                self.pendingDisconnects[shredPeripheral] = pending
                
                self.cbCentralManager.cancelPeripheralConnection(shredPeripheral.cbPeripheral)
            }
        }
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        Task {
            await self.central.bridgeDidUpdateState(state: central.state)
        }
    }
    
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        BlueShred.serialQueue.async {
            let shredPeripheral = ShredPeripheral(peripheral: peripheral, identifier: peripheral.identifier, advertisementData: advertisementData)
            
            self.central.delegate.didDiscoverPeripheral(peripheral: shredPeripheral, rssi: RSSI)
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        BlueShred.serialQueue.async {
            guard let shredPeripheral = self.pendingConnections.keys.filter({ $0.cbPeripheral.identifier == peripheral.identifier }).first,
                  let pendingConnection = self.pendingConnections[shredPeripheral] else {
                // TODO: a delegate callback for this case is probably necessary
                Log.blueShred.error("Did connect to peripheral, but we have no pending connection operation")
                return
            }
            
            Log.blueShred.debug("Did connect to peripheral: \(shredPeripheral, privacy: .public)")
            
            self.pendingConnections.removeValue(forKey: shredPeripheral)

            // TODO: maybe our discovery and connected methods should use a different data model?
            shredPeripheral.cbPeripheral = peripheral
            pendingConnection.complete(returning: ())
        }
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        BlueShred.serialQueue.async {
            Log.blueShred.debug("Did disconnect from peripheral: \(peripheral.identifier)")
            guard let shredPeripheral = self.pendingDisconnects.keys.filter({ $0.cbPeripheral.identifier == peripheral.identifier }).first,
                  let pending = self.pendingDisconnects[shredPeripheral] else {
                Log.blueShred.error("Did disconnect from peripheral: \(peripheral.identifier, privacy: .public), but we have not pending disconnect operation")
                return
            }
            
            self.pendingDisconnects.removeValue(forKey: shredPeripheral)
            
            pending.complete(returning: ())
        }
    }
    
    public func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
        Log.blueShred.notice("Connection event: \(event.rawValue, privacy: .public) occurred for peripheral: \(peripheral.identifier.uuidString, privacy: .public)")
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        // TODO: look for the continuation and resume
        Log.blueShred.error("Failed to connect to peripheral: \(peripheral, privacy: .public). Subclass should override this method and handle the error appropriately")
    }
    
}

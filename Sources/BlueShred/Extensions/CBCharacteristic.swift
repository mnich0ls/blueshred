//
//  CBCharacteristic.swift
//  
//
//  Created by Michael Nichols on 3/9/23.
//

import CoreBluetooth

extension CBCharacteristic {
    
    func shredCharacteristic() -> ShredCharacteristic? {
        guard let service = service else {
            Log.blueShred.error("Expected service for characteristic: \(self.uuid.uuidString, privacy: .public)")
            return nil
        }
        return ShredCharacteristic(serviceUUID: service.uuid.uuidString, charUUID: uuid.uuidString)
    }
}

//
//  BlueShred.swift
//  
//
//  Created by Michael Nichols on 3/9/23.
//

import Foundation

public struct BlueShred {
    
    public static var subsystem = "dev.pacificrose.blueshred"
    
    public static var serialQueue = DispatchQueue(label: subsystem)
}

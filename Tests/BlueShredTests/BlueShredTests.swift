import XCTest
@testable import BlueShred

final class BlueShredTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(BlueShred().text, "Hello, World!")
    }
}

# BlueShred

A simple wrapper around the CBCentralManager and CBPeripheral CoreBluetooth classes, providing an asynchronous interface to connect and read or write characteristics. 
